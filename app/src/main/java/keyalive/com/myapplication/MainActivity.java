package keyalive.com.myapplication;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, FetchAddressTask.OnTaskCompleted {

    private static final int REQ_LOCATION_PERMISION = 1;
    private TextView txtLocation;
    private Button btnLocation;
    FusedLocationProviderClient fusedLocationProviderClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        initView();
        btnLocation.setOnClickListener(this);
    }

    private void initView() {
        txtLocation = (TextView) findViewById(R.id.txtLocation);
        btnLocation = (Button) findViewById(R.id.btn_location);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_location:
                getLocation();
                break;
        }
    }

    private void getLocation() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{
                    Manifest.permission.ACCESS_FINE_LOCATION
            }, REQ_LOCATION_PERMISION);

        } else {
//          Lokasi tempat kita berdiri dalam 1 titik
            fusedLocationProviderClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
//                    if (location!=null){
//                        txtLocation.setText("Latitude:" + location.getLatitude() +
//                                "\n longitude:"+ location.getLongitude());
//                    }

                }

            });
//            Lokasi kita saat berjalan
            fusedLocationProviderClient.requestLocationUpdates( getLocationRequest(), new LocationCallback(){
                @Override
                public void onLocationResult(LocationResult locationResult){
                    new FetchAddressTask(MainActivity.this,
                            MainActivity.this)
                            .execute(locationResult.getLastLocation());
                }
            },null);
        }
    }
    private LocationRequest getLocationRequest(){
        LocationRequest locationRequest=new LocationRequest();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return locationRequest;
    }

        @Override
        public void onRequestPermissionsResult ( int requestCode, @NonNull String[] permissions,
        @NonNull int[] grantResults){
            switch (requestCode) {
                case REQ_LOCATION_PERMISION:
                    if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_DENIED) {
                        getLocation();
                    } else {
                        Toast.makeText(this, "KASIAN DEH LU DITOLAK", Toast.LENGTH_SHORT).show();
                    }
                    break;
            }
        }


    @Override
    public void onTaskCompleted(String result) {
        txtLocation.setText(result);
    }
}

