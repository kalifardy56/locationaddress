package keyalive.com.myapplication;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.AsyncTask;
import android.text.TextUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class FetchAddressTask extends AsyncTask<Location,Void,String> {
    private Context mcontext;
    private OnTaskCompleted mlistener;
    FetchAddressTask(Context context, OnTaskCompleted listener){
        mcontext=context;
        mlistener=listener;
    }

    @Override
    protected String doInBackground(Location... locations) {
        Geocoder geocoder= new Geocoder(
                mcontext, Locale.getDefault());
                Location location= locations[0];
        List<Address>addresses=null;
        String resultMessage="";
        try {
            addresses=geocoder.getFromLocation(location.getLatitude(),location.getLongitude(),1);

        } catch (IOException e) {
            e.printStackTrace();
        }

        if (addresses==null||addresses.size()==0){
            resultMessage="ALAMAT TIDAK DI TEMUKAN";

        }
        else {
            Address address=addresses.get(0);
            ArrayList<String>addressParts=new ArrayList<>();
            for (int i=0 ;i <=address.getMaxAddressLineIndex();i++){
                addressParts.add(address.getAddressLine(i));
            }
            resultMessage= TextUtils.join("\n",addressParts);
        }
        return resultMessage;
    }

    @Override
    protected void onPostExecute(String s) {
        mlistener.onTaskCompleted(s);
        super.onPostExecute(s);
    }

    interface OnTaskCompleted{
        void onTaskCompleted(String result);
    }
}
